/* JokerBoy - http://hg.punctweb.ro */

/* appearance */
#define BARSHRINK	530		// by how many pixels to shrink the top bar - not used now - SM
#define BARWIDTH	1150	// bar width is used, otherwise dzen gadgets "disappear" in wide screens - SM
//#define BARWIDTH	2800	// bar width for 4k display

//#define PIDGIN_LEFT		"Buddy List"
#define PIDGIN_LEFT		"Buddy"
//#define PIDGIN_RIGHT	"smanucharyan - Skype"
#define PIDGIN_RIGHT	"fb"
#define PIDGIN_RWIDTH	900
#define PIDGIN_LWIDTH	240

//#define GIMP_LEFT		"Toolbox - Tool Options"
#define GIMP_LEFT		"Toolbox"
//#define GIMP_RIGHT		"Layers - Brushes"
#define GIMP_RIGHT		"Layers"
#define GIMP_RWIDTH		240
#define GIMP_LWIDTH		240

//static const char font[]            = "-*-terminus-medium-r-*-*-28-*-*-*-*-*-*-*";
static const char font[]            = "dejavu sans mono:size=9";

static const char colors[MAXCOLORS][ColLast][8] = {
	/* border     fg         bg       */
	//{ "#5555AA", "#888888", "#191970" }, /* 0 = normal */
	{ "#5555AA", "#FFFFFF", "#191970" }, /* 0 = normal */
	{ "#FF0000", "#FFFFFF", "#FF6600" }, /* 1 = selected */
	{ "#FF2882", "#FFFFFF", "#FF2882" }, /* 2 = urgent */
	{ "#222222", "#53A6A6", "#1A1A1A" }, /* 3 = green */
	{ "#222222", "#BF85CC", "#1A1A1A" }, /* 4 = yellow */
	{ "#222222", "#6096BF", "#1A1A1A" }, /* 5 = cyan */
	{ "#222222", "#7E62B3", "#1A1A1A" }, /* 6 = magenta */
	{ "#222222", "#899CA1", "#1A1A1A" }, /* 7 = grey */
};
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 8;        /* snap pixel */
static const Bool showbar           = True;     /* False means no bar */
static const Bool showsystray       = True;     /* False means no systray */
static const Bool topbar            = True;     /* False means bottom bar */
static const Bool clicktofocus      = False;     /* Change focus only on click */
static const Bool viewontag         = True;     /* Switch view on tag switch */

static const Rule rules[] = {
  /* class          instance  title           role     tags mask    isfloating iscentred monitor */
  { "Gimp",         NULL,     NULL,           NULL,         1 << 5,   False,     False,   -1 },
  { "VCLSalFrame",  NULL,     NULL,           NULL,         1 << 7,   False,     False,   -1 },
  { NULL,           NULL,     "LibreOffice",  NULL,         1 << 7,   False,     False,   -1 },
  { NULL,           NULL,     "Yate Client",  NULL,         1 << 4,   False,     False,   -1 },
  { NULL,           NULL, "Messenger Call - Chromium", 0,   False,    False,      -1 },
  { NULL,           NULL,     "Accounts",     NULL,         0,        True,      True,    -1 },
  { "XFontSel",     NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "XClock",       NULL,      NULL,          0,            True,        True,       -1 },
  { "Gnuplot",      NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "Galculator",   NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "Linphone",     NULL,     NULL,           NULL,         1 << 4,   False,     False,   -1 },
  { "feh",          NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "Wine",         NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "MPlayer",      NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "mpv",          NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { NULL,           NULL,     "mplayer2",     NULL,         0,        True,      True,    -1 },
  { "Zenity",       NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "Bcompare",     NULL,     NULL,           NULL,         0,        True,      True,    -1 },
  { "ffplay",       NULL,      NULL,          0,            True,        True,       -1 },
  { "Firefox",      NULL,     NULL,           NULL,         1 << 1,   False,     True,    -1 },
  { "Chromium-browser",  NULL, NULL,          NULL,         1 << 1,   False,     True,    -1 },
  { "Thunderbird",  NULL,     NULL,           NULL,         1 << 2,   False,     True,     0 },
  { NULL,           NULL,     "Downloads",    NULL,         1 << 1,   True,      False,   -1 },
  { NULL,           NULL,     "dzen-dialog",  NULL,         0,        True,      True,    -1 },
  { "weather",      NULL,     NULL,	      NULL,         0,        True,      True,    -1 },
  { "rdesktop",     NULL,     NULL,           NULL,         1 << 9,   False,     False,    1 },
  { "xfreerdp",     NULL,     NULL,           NULL,         1 << 9,   False,     False,    1 },
  { "TigerVNC Viewer",  NULL, NULL,           NULL,         0,        True,      True,    -1 },
  { "Pidgin",       NULL,     NULL,           NULL,         1 << 4,   False,     False,    1 },
  { "firefox",      NULL,     NULL,           NULL,         1 << 1,   False,     False,    1 },
  { "Chromium",     NULL,     NULL,           NULL,         1 << 1,   False,     False,    1 },
  { NULL,           NULL,     NULL, "GtkFileChooserDialog", 0,        True,      True,    -1 },
};

/* layout(s) */
static const float mfact      = 0.5;   /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;     /* number of clients in master area */
static const Bool resizehints = False; /* True means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[T]",      tile },        /* first entry is default */
	{ "[M]",      monocle },
	{ "[F]",      NULL },        /* no layout function means floating behavior */
	{ "[B]",      bstack },
	{ "[G]",      gaplessgrid },
	{ "[P]",      pidgin },
	{ "[I]",      gimp },
};

/* tagging */
static const Tag tags[] = {
	/* name    layout           mfact    nmaster */
	{ " 1",     &layouts[0],     -1,      -1 },
	{ " 2",     &layouts[1],     -1,      -1 },
	{ " 3",     &layouts[1],     -1,      -1 },
	{ " 4",     &layouts[4],     -1,      -1 },
	{ " 5",     &layouts[5],     -1,      -1 },
	{ " 6",     &layouts[0],     -1,      -1 },
	{ " 7",     &layouts[0],     -1,      -1 },
	{ " 8",     &layouts[1],     -1,      -1 },
	{ " 9",     &layouts[0],     -1,      -1 },
	{ " A",     &layouts[4],     -1,      -1 },
	{ " B",     &layouts[0],     -1,      -1 },
	{ " C",     &layouts[2],     -1,      -1 },
};

/* key definitions */
#define ALTKEY Mod1Mask
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
/* commands */
#ifdef SCRATCHY_SIZE_SMALL
#define SCRATCHY_GEOM	"130x32"
#else
#define SCRATCHY_GEOM	"150x40"
#endif

static const char terminal[]        = "urxvt";
static const char scratchpadname[]  = "scratchy";
static const char *terminalcmd[]    = {"urxvt", NULL};
static const char *dmenucmd[]       = { "dmenu_run", "-nf", "blue", "-nb", "white", "-sb", "lightblue", "-sf", "black", NULL };
static const char *scratchpadcmd[]  = { terminal, "-name", scratchpadname, "-geometry", SCRATCHY_GEOM, NULL };
static const char *browserfcmd[]    = { "browser", NULL };
static const char *emailcmd[]       = { "email", NULL };
static const char *imcmd[]          = { "pidgin", NULL };
static const char *skypecmd[]       = { "skype", NULL };
static const char *calccmd[]        = { "galculator", NULL };
// currently PrtScr is implemented via acpid with Fn + F1
static const char *screenshotcmd[]  = { "/home/sergeym/bin/printscreen", NULL };
static const char *engcmd[]         = { "/home/sergeym/scripts/xkb", "0", NULL };
static const char *armcmd[]         = { "/home/sergeym/scripts/xkb", "2", NULL };
static const char *ruscmd[]         = { "/home/sergeym/scripts/xkb", "1", NULL };
static const char *intlcmd[]        = { "/home/sergeym/scripts/us-intl", NULL };
static const char *loudercmd[]      = { "volume-louder", NULL };
static const char *quietercmd[]     = { "volume-quieter", NULL };
//static const char *mutecmd[]        = {  "volume-mute", NULL };
static const char *micmutecmd[]     = {  "volume-mic-mute", NULL };
static const char *playnextcmd[]    = {  "player-next", NULL };
static const char *playprevcmd[]    = {  "player-prev", NULL };
static const char *playstartcmd[]   = {  "player-start", NULL };
static const char *playstopcmd[]    = {  "player-stop", NULL };
static const char *extmoncmd[]      = {  "dp-on", NULL };		// display port


static Key keys[] = {
	/* modifier                     key         function        argument */
	{ MODKEY,                       XK_p,       spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_w,       spawn,          {.v = browserfcmd } },
	{ MODKEY|ShiftMask,             XK_e,       spawn,          {.v = emailcmd } },
	{ MODKEY,                       XK_Return,  spawn,          {.v = terminalcmd } },
	{ MODKEY,                       XK_x,       togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY|ShiftMask,             XK_p,       spawn,          {.v = imcmd } },
	{ MODKEY|ShiftMask,             XK_s,       spawn,          {.v = skypecmd } },
	{ MODKEY|ShiftMask,             XK_c,       spawn,          {.v = calccmd } },
	{ 0, 		                    XK_Print,   spawn,          {.v = screenshotcmd } },
	{ MODKEY|ShiftMask,             XK_m,       togglemax,      {0} },
	{ MODKEY|ShiftMask,             XK_b,       togglebar,      {0} },
	{ MODKEY,                       XK_Tab,     focusstack,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_Tab,     focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j,       pushdown,       {0} },
	{ MODKEY|ShiftMask,             XK_k,       pushup,         {0} },
	{ MODKEY,                       XK_a,       incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_z,       incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,       setmfact,       {.f = -0.01} },
	{ MODKEY,                       XK_l,       setmfact,       {.f = +0.01} },
	{ ALTKEY,                       XK_Return,  zoom,           {0} },
	{ MODKEY,                       XK_Escape,  view,           {0} },
	{ MODKEY,                       XK_Tab,     focusurgent,    {0} },
	{ ALTKEY,                       XK_F4,      killclient,     {0} },
	{ MODKEY|ControlMask,           XK_c,       centerwindow,   {0} },
	{ MODKEY|ControlMask,           XK_t,       setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ControlMask,           XK_m,       setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ControlMask,           XK_f,       setlayout,      {.v = &layouts[2]} },
	{ MODKEY|ControlMask,           XK_b,       setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ControlMask,           XK_g,       setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ControlMask,           XK_p,       setlayout,      {.v = &layouts[5]} },
	{ MODKEY,                       XK_space,   setlayout,      {0} },
	{ MODKEY,                       XK_t,       togglefloating, {0} },
	{ MODKEY,                       XK_0,       view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,       tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,   focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period,  focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,   tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,  tagmon,         {.i = +1 } },
	{ MODKEY,                       XK_Left,    cycle,          {.i = -1 } },
	{ MODKEY,                       XK_Right,   cycle,          {.i = +1 } },
	{ MODKEY|ControlMask,           XK_Left,    tagcycle,       {.i = -1 } },
	{ MODKEY|ControlMask,           XK_Right,   tagcycle,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_q,       quit,           {0} },
	{ 0,                            XK_F20,     spawn,          {.v = engcmd } },
	{ 0|ShiftMask,                  XK_F20,     spawn,          {.v = armcmd } },
	{ 0|ControlMask,                XK_F20,     spawn,          {.v = ruscmd } },
	{ 0|ShiftMask|ControlMask,      XK_F20,     spawn,          {.v = intlcmd } },
	{ 0|ControlMask,                XK_Escape,  spawn,          {.v = intlcmd } },
	//{ 0,                            0x1008ff13, spawn,          {.v = loudercmd  } },
	//{ 0,                            0x1008ff11, spawn,          {.v = quietercmd } },
	//{ 0,                            0x1008ff12, spawn,          {.v = mutecmd    } },
	//{ 0,                            0x1008ffb2, spawn,          {.v = micmutecmd } },
	//{ 0,                            0x1008ff17, spawn,          {.v = playnextcmd } },
	//{ 0,                            0x1008ff16, spawn,          {.v = playprevcmd } },
	//{ 0,                            0x1008ff14, spawn,          {.v = playstartcmd} },
	//{ 0,                            0x1008ff15, spawn,          {.v = playstopcmd } },
	//{ 0,                            0x1008ff8f, spawn,          {.v = extmoncmd } },
	TAGKEYS(                        XK_F1,                       0)
	TAGKEYS(                        XK_F2,                       1)
	TAGKEYS(                        XK_F3,                       2)
	TAGKEYS(                        XK_F4,                       3)
	TAGKEYS(                        XK_F5,                       4)
	TAGKEYS(                        XK_F6,                       5)
	TAGKEYS(                        XK_F7,                       6)
	TAGKEYS(                        XK_F8,                       7)
	TAGKEYS(                        XK_F9,                       8)
	TAGKEYS(                        XK_F10,                      9)
	TAGKEYS(                        XK_F11,                     10)
	TAGKEYS(                        XK_F12,                     11)
};

/* button definitions */
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click            event mask        button          function        argument */
	{ ClkLtSymbol,      0,                Button1,        setlayout,      {0} },
	{ ClkClientWin,     ALTKEY,           Button1,        movemouse,      {0} },
	{ ClkClientWin,     MODKEY,           Button2,        togglefloating, {0} },
	{ ClkClientWin,     ALTKEY|ShiftMask, Button1,        resizemouse,    {0} },
	{ ClkWinTitle,      0,                Button4,        focusstack,     {.i = +1} },
	{ ClkWinTitle,      0,                Button5,        focusstack,     {.i = -1} },
	{ ClkTagBar,        0,                Button1,        view,           {0} },
	{ ClkTagBar,        0,                Button3,        toggleview,     {0} },
	{ ClkTagBar,        MODKEY,           Button1,        tag,            {0} },
	{ ClkTagBar,        MODKEY,           Button3,        toggletag,      {0} },
	{ ClkTagBar,        0,                Button4,        cycle,          {.i = -1} },
	{ ClkTagBar,        0,                Button5,        cycle,          {.i = +1} },
};

